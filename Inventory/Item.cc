#include "Item.hh"

Item::Item(string n, int b, int w) : name(n), volume(b), weight(w) {

}

int Item::get_volume() const {
  return this->volume;
}

int Item::get_weight() const {
  return this->weight;
}

// Usage with parameter names as in signature: os << i
ostream& operator<<(ostream& os, const Item& i) {
  os << "- " << i.name << " (" << i.volume << " litre, "
     << i.weight << " g)" << endl;
  return os;
}
