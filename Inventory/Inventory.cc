#include "Inventory.hh"

Inventory::Inventory(int s, int c): space(s), capacity(c), hands(AXE) {}

Inventory::Inventory(int s, int c, const Item& h): space(s), capacity(c), hands(h) {}

int Inventory::used_space() const {
  auto unary_op = [](Item item){return item.get_volume();};
  auto binary_op = [](int num1, int num2){return num1 + num2;};

  return std::transform_reduce(
      this->items.begin(), // Start of array section
      this->items.end(),   // End of array section
      0,                   // Initial zero sum
      binary_op,           // Addition
      unary_op             // Get volume
      );
}

int Inventory::used_capacity() const {
  auto unary_op = [](Item item){return item.get_weight();};
  auto binary_op = [](int num1, int num2){return num1 + num2;};

  return std::transform_reduce(
      this->items.begin(),      // Start of array section
      this->items.end(),        // End of array section
      this->hands.get_weight(), // Initial zero sum
      binary_op,                // Addition
      unary_op                  // Get weight
      );
}

int Inventory::free_space() const {
  return this->space - this->used_space();
}

int Inventory::free_capacity() const {
  return this->capacity - this->used_capacity();
}

bool Inventory::pickup(const Item& i) {
  if (this->free_space() >= i.get_volume() && 
      this->free_capacity() >= i.get_weight()) {
    items.push_back(i);
    return true;
  } else {
    return false;
  }
}

bool Inventory::drop(unsigned int idx) {
  if (idx >= items.size()) {
    return false;
  } else {
    items.erase(items.begin() + idx);
    return true;
  }
}

bool Inventory::swap(unsigned int idx) {
  if (idx >= items.size()) {
    return false;
  }

  Item new_item = items[idx];

  if (this->free_space() + new_item.get_volume() >= this->hands.get_volume()) {
    items.erase(items.begin() + idx);
    this->items.push_back(this->hands);
    this->hands = new_item;
    return true;
  }

  return false;

}

ostream& operator<<(ostream& os, const Inventory& inv) {
  os << "Space: " << inv.free_space() << "/" << inv.space << endl
     << "Capacity: " << inv.free_capacity() << "/" << inv.capacity << endl
     << "Wielding" << endl << inv.hands
     << "Backpack" << endl;

  for (Item i : inv.items) {
    os << i;
  }

  return os << endl;
}
