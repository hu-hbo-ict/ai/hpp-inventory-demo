#ifndef ITEM_HH
#define ITEM_HH

#include <iostream>

using std::string, std::ostream, std::endl;

class Item  {
  private:
    string name;
    int volume;
    int weight;
  public:
    Item(string n, int b, int w);
    int get_volume() const;
    int get_weight() const;
    friend ostream& operator<<(ostream& os, const Item& i);
};

#define AXE Item("Axe", 6, 2000)

#endif
