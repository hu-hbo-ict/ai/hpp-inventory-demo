#pragma once

#include "Item.hh"
#include <numeric>
#include <vector>

using std::vector;

class Inventory {
  private:
    int space;
    int capacity;
    Item hands;
    vector<Item> items;
  public:
    Inventory(int s, int c);
    Inventory(int s, int c, const Item& h);
    int used_space() const;
    int used_capacity() const;
    int free_space() const;
    int free_capacity() const;
    bool pickup(const Item& i);
    bool swap(unsigned int idx);
    bool drop(unsigned int idx);
    friend ostream& operator<<(ostream& os, const Inventory& i);
};
