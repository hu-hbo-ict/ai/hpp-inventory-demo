#include <iostream>
#include "Inventory.hh"
#include "Item.hh"

using std::cout;

int main() {
  auto axe = Item("Axe", 6, 2000);
  auto shovel = Item("Shovel", 6, 2000);
  auto rod = Item("Fishing rod", 4, 500);
  auto papaya = Item("Papaya", 1, 200);

  cout << axe << endl;

  auto i = Inventory(30, 4000, axe);
  cout << i;

  i.pickup(shovel);
  cout << i;

  i.pickup(papaya);
  i.pickup(papaya);
  i.pickup(papaya);
  cout << i;
  i.drop(1);
  cout << i;

  auto k = Inventory(10, 10000);

  k.pickup(rod);
  k.pickup(shovel);
  cout << k;
  k.swap(1);
  cout << k;
  k.swap(0);
  cout << k;
  k.drop(1);
  cout << k;
  k.swap(0);
  cout << k;
}

