#include <iostream>
#include <memory>
#include <cmath>
#include "Library.hh"

using std::cout, std::endl;

// Psych, no leak
void leak() {
  int a = 1;
  cout << &a << " : " << a << endl;
}

void leak2() { // Now using pointer and heap allocation
  int* a = new int(2);
  cout << a << " : " << *a << endl;
  // memory leak
}

void leak3() {
  int* a = new int(3);
  cout << a << " : " << *a << endl;
  delete(a);  // fixed
}

// Harder to spot:
int leak4() {
  int* a = new int(4);
  cout << a << " : " << *a << endl;
  bool cond = true;

  // ...

  if (cond) {
    return 0;
  }

  delete(a);
  return -1;
}

// Even worse: Exceptions (omitted)

// Possible solution: Smarter pointers
void leak5() {
    std::unique_ptr<int> a(new int(5));
    cout << a.get() << " : " << *a << endl;
}

// warning: 'b' is used uninitialised
void wild() {
  int *b;
  cout << "This one's wild: " << b << endl;
}

// warning: address of local variable 'b' returned
int* dangle() {
  int b = 42;
  return &b;
}

// No effect. Fix: Library& instead of Library
// Will not compile if Library inherits noncopyable
void acquire_book(Library l, string b) {
  l.add_book(b);
}

// Any calls to these functions will be done at compile time if possible - if arguments available at compile.
constexpr double pi = std::acos(-1);
constexpr double sinus(int angle) {
  return std::sin(angle * pi / 180);
}

int main() {
  // Dangling / Wild Pointers
  /*
  int* dp = dangle();
  cout << "Dangling: " << dp << endl;

  wild();
  */

  // Memory Leaks
  /*
  leak();
  leak2();
  leak3();
  leak4();
  leak5();
  */

  /*
  auto l = Library("De bieb");
  cout << l << endl;
  l.add_book("Bjarne Stroustrup - The C++ Programming Language");
  cout << l << endl;
  acquire_book(l, "Ken Arnold James Gosling, David Holmes - The Java™ Programming Language");
  cout << l << endl;
  */

  // Constant expressions, calculated at compile time
  /*
  cout << sinus(0) << endl;
  cout << sinus(90) << endl;
  cout << sinus(180) << endl;
  cout << sinus(270) << endl;
  */
}
