#include "Library.hh"

using std::string, std::vector, std::cout, std::endl;

Library::Library(string s) : name(s) { }

void Library::add_book(string b) {
  this->books.push_back(b);
}

std::ostream& operator<<(std::ostream& os, Library& l) {
  os << "Library { " << endl;
  for (string b : l.books) {
    os << b << endl;
  }
  os << "}" << endl << endl;
  return os;
}
