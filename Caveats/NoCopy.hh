#ifndef NO_COPY
#define NO_COPY

class noncopyable {
  public:
      noncopyable() {}
  private:
      noncopyable( const noncopyable& );
      noncopyable& operator=( const noncopyable& );
  };
#endif

