#pragma once

#include <iostream>
#include <vector>
#include "NoCopy.hh"

using std::string, std::vector;

// class Library : noncopyable { // compile-time error on copy
class Library { // allow accidental copy
  private:
    string name;
    vector<string> books;
  public:
    Library(string s);
    void add_book(string b);
    friend std::ostream& operator<<(std::ostream& os, Library& l);
};
